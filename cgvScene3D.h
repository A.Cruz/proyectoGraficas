#ifndef __CGVSCENE3D
#define __CGVSCENE3D

#if defined(__APPLE__) && defined(__MACH__)
#include <GLUT/glut.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/glut.h>
#endif

typedef enum {
	CGV_VISUALIZE,
	CGV_SELECT
} renderMode;

class cgvScene3D {
	protected:
////// TODO: Section B: add here the attributes to control the degrees of freedom of the model

		bool armsRotated;
		int rotatingArms;

		bool legDir;
		int rotatingLegs;

		int xRotHead = 0;
		int yRotHead = 0;
		int zRotHead = 0;

		bool xDirRotHead;
		bool yDirRotHead;
		bool zDirRotHead;

		double noseFactor;

////// TODO: Section D: add here the attribute/s to control the selected object and to color it yellow


////// TODO: Section D: Add here the attribute/s to control the color assigned to each moving part of the model. 

		// 0 - LEG1 | 1 - LEG2 | 2 - ARM1 | 3 - ARM2 | 4 - BODY | 5 - HEAD | 6 - NOSE | 7 - HAT
		float objectColor[8][3];
		int selectedObject;

		bool showBoundingBox;
		// Additional attributes		

		bool axes;		

	public:
		// Default constructor and destructor
		cgvScene3D();
		~cgvScene3D();

		// Static methods

		// Methods
		// method with the OpenGL calls to render the scene
    void render(renderMode mode);
	
	////// TODO: Section B: include here the methods to modify the degrees of freedom of the model
		int get_rotX() { return xRotHead; };
		void incr_rotX(int angle) {
			if (xDirRotHead) 
				xRotHead += angle;
			if (xRotHead >= 70)
				xDirRotHead = false;
			
			if(!xDirRotHead) 
				xRotHead -= angle;
			if (xRotHead <= -70)
				xDirRotHead = true;
			
		};

		void incr_rotX2(int angle) {
			if (xRotHead + angle < 70 && xRotHead + angle > -70)
				xRotHead += angle;

		};

		int get_rotY() { return yRotHead; };
		void incr_rotY(int angle) {
			if (yDirRotHead)
				yRotHead += angle;
			if (yRotHead >= 70)
				yDirRotHead = false;

			if (!yDirRotHead)
				yRotHead -= angle;
			if (yRotHead <= -70)
				yDirRotHead = true;
		};

		void incr_rotY2(int angle) {
			if (yRotHead + angle < 70 && yRotHead + angle > -70)
				yRotHead += angle;

		};

		int get_rotZ() { return zRotHead; };
		void incr_rotZ(int angle) {
			if (zDirRotHead) {
				zRotHead += angle;
				if (zRotHead == 70)
					zDirRotHead = false;
			}
			else {
				zRotHead -= angle;
				if (zRotHead == -70)
					zDirRotHead = true;
			}
		};

		bool get_armsRotated() { return armsRotated; };
		void set_armsRotated(bool _armsRotated) { armsRotated = _armsRotated; };

		int get_rotatingArms() { return rotatingArms; };
		void incr_rotatingArms(int angle) { rotatingArms += angle; };

		void incr_rotatingLegs(int angle) { rotatingLegs += angle; };
		int get_rotatingLegs() { return rotatingLegs; };

		bool get_legDir() { return legDir; };
		void set_legDir(bool _legDir) { legDir = _legDir; };

		void change_noseFactor() {
			if (noseFactor >= 1)
				noseFactor = 0.5;
			else
				noseFactor = 1;
		};

		void incr_noseFactor(float incr) {
			if (noseFactor >= 1 && incr < 0)
				noseFactor += incr/100;
			else if (noseFactor <= 0 && incr > 0)
				noseFactor += incr/100;
			else if(noseFactor <= 1 && noseFactor >= 0)
				noseFactor += incr/100;
		};

		void arm1(renderMode mode);
		void arm2(renderMode mode);
		void leg1(renderMode mode);
		void leg2(renderMode mode);
		void body(renderMode mode);
		void head(renderMode mode);
		void hat(renderMode mode);
		void nose(renderMode mode);

		bool get_showBoundingBox() { return showBoundingBox; };
		void set_showBoundingBox(bool _showBoundingBox) { showBoundingBox = _showBoundingBox; };
	
	////// Section D: This method is called to compute the selected object
        void assignSelection (GLfloat color[3]); 
        
		bool get_axes() {return axes;};
		void set_axes(bool _axes){axes = _axes;};

		int get_selectedObject() { return selectedObject; };
		void set_selectedObject(int _selectedObject) { selectedObject =  _selectedObject; };
		
};

#endif
