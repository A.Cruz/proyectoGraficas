#include <cstdlib>
#include <stdio.h>

#include "cgvScene3D.h"

static GLfloat selected_color[]={1,1,0,1.0};

// Constructor methods -----------------------------------

cgvScene3D::cgvScene3D () {
	axes = true;

	
// TODO: Section B: initialize the attributes to control the degrees of freedom of the model
        
	armsRotated = false;
	rotatingArms = 0;

	rotatingLegs = 0;
	legDir = false;

	noseFactor = 0.5;

	xRotHead = 0;
	yRotHead = 0;
	zRotHead = 0;

	xDirRotHead = false;
	yDirRotHead = false;
	zDirRotHead = false;

	showBoundingBox = false;


////// TODO: Section D: o	Initialize the attribute/s to control the color assigned to each moving part of the model.

	// 0 - LEG1 | 1 - LEG2 | 2 - ARM1 | 3 - ARM2 | 4 - BODY | 5 - HEAD | 6 - NOSE | 7 - HAT
	objectColor[0][0] = 0;
	objectColor[0][1] = 1;
	objectColor[0][2] = 0.2;

	objectColor[1][0] = 0.2;
	objectColor[1][1] = 1;
	objectColor[1][2] = 0.2;

	objectColor[2][0] = 1;
	objectColor[2][1] = 0;
	objectColor[2][2] = 0.2;

	objectColor[3][0] = 1;
	objectColor[3][1] = 0;
	objectColor[3][2] = 0;

	objectColor[4][0] = 0;
	objectColor[4][1] = 0;
	objectColor[4][2] = 1;

	objectColor[5][0] = 0;
	objectColor[5][1] = 1;
	objectColor[5][2] = 0;

	objectColor[6][0] = 0;
	objectColor[6][1] = 1;
	objectColor[6][2] = 1;

	objectColor[7][0] = 1;
	objectColor[7][1] = 0;
	objectColor[7][2] = 1;


	selectedObject = -2;

///// TODO: Section D: initialize the attribute/s that identifies the selected object and to colour it yellow

}

cgvScene3D::~cgvScene3D() {}


// Public methods ----------------------------------------

void draw_axes(void) {
  GLfloat red[]={1,0,0,1.0};
  GLfloat green[]={0,1,0,1.0};
  GLfloat blue[]={0,0,1,1.0};

	glBegin(GL_LINES);
    glMaterialfv(GL_FRONT,GL_EMISSION,red);
		glVertex3f(1000,0,0);
		glVertex3f(-1000,0,0);

    glMaterialfv(GL_FRONT,GL_EMISSION,green);
		glVertex3f(0,1000,0);
		glVertex3f(0,-1000,0);

    glMaterialfv(GL_FRONT,GL_EMISSION,blue);
		glVertex3f(0,0,1000);
		glVertex3f(0,0,-1000);
	glEnd();
}


void cgvScene3D::render(renderMode mode) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the window and the z-buffer
	glEnable(GL_COLOR_MATERIAL);
	// lights
	GLfloat light0[4]={5.0,5.0,5.0,1}; // point light source  
	glLightfv(GL_LIGHT0,GL_POSITION,light0); // this light is placed here and it remains still 
    glEnable(GL_LIGHT0);
  
	// create the model
	glPushMatrix(); // store the model matrices

	  // draw the axes
	if ((axes)&&(mode == CGV_VISUALIZE)) draw_axes();

	float mesh_color[4] = {0.0, 0.0, 0.0, 1.0};
	glMaterialfv(GL_FRONT,GL_EMISSION,mesh_color);

///// TODO: Section B: include here the visualization of the tree of the model by using the OpenGL stack of matrices 
/////           it is advisable to create an auxiliary method to encapsulate the code to visualize the model
/////           leaving here only the call to this method. 

	
	glTranslated(0, 2.5, 0);
	glRotated(90,1,0,0);
	

	glPushMatrix();
		glTranslated(0, 0, 2.5);

		//ARM 1
		glPushMatrix();
		
			arm1(mode);

		glPopMatrix();

	

		//Arm 2
		glPushMatrix();

			arm2(mode);

		glPopMatrix();

		glPushMatrix();		
		
			glTranslated(0, 0, 2.5);

			//Rotation of legs in de axis Y
			glRotated(rotatingLegs, 0, 1, 0);

			//Leg 1
			glPushMatrix();
				
				leg1(mode);

			glPopMatrix();

			//Leg 2
			glPushMatrix();

				leg2(mode);

			glPopMatrix();

		glPopMatrix();

	glPopMatrix();


	//Body
	glPushMatrix();
		body(mode);
	glPopMatrix();


	glPushMatrix();

		glRotated(-90, 1, 0, 0);
		glRotated(xRotHead, 1, 0, 0);
		glRotated(yRotHead, 0, 1, 0);
		glRotated(zRotHead, 0, 0, 1);

		//Hat
		glPushMatrix();
			
			hat(mode);

		glPopMatrix();

		//Nose
		glPushMatrix();
			nose(mode);
		glPopMatrix();

	glPopMatrix();

		//head
		
		head(mode);
		
	


///// TODO: Section D: Define a bounding box for each part of the hierarchical model. 

///// TODO: Section D: When in selection mode use the function glColor3f instead of glMaterialfv to define the color of the parts of the model. 

		
// TODO: Section E: When in selection mode draw the bounding box of each element of the model instead of the real geometry
	
	glPopMatrix (); // restore the modelview matrix 

}

void cgvScene3D::assignSelection(GLfloat color[3]) {
	// TODO: Section D: Compute the selected object taking into account the color detected in a given position of the window. 
	for (int i = 0; i < 8 && selectedObject<0; i++) {
		bool correctColor = true;
		for (int j = 0; j < 3 && correctColor; j++) {
			if (objectColor[i][j] != color[j]) {
				correctColor = false;
			}
		}
		if (correctColor)
			selectedObject = i;
	}


}


void cgvScene3D::leg1(renderMode mode) {
	int objectNumber = 0;
	if(selectedObject== objectNumber)
		glColor3f(255.0f, 255.0f, 0.0f);
	else
		glColor3f(objectColor[objectNumber][0], objectColor[objectNumber][1], objectColor[objectNumber][2]);

	glRotatef(45.0f, 0.0f, 1.0f, 0.0f);
	
	if (mode == CGV_VISUALIZE && !showBoundingBox)
		gluCylinder(gluNewQuadric(), 0.4f, 0.4f, 3.0f, 32, 32);
	else {
		glScaled(1, 1, 3);
		glTranslated(0, 0, 0.5);
		glutSolidCube(1);
	}

}

void cgvScene3D::leg2(renderMode mode) {
	int objectNumber = 1;
	if (selectedObject == objectNumber)
		glColor3f(255.0f, 255.0f, 0.0f);
	else
		glColor3f(objectColor[objectNumber][0], objectColor[objectNumber][1], objectColor[objectNumber][2]);

	glRotatef(-45.0f, 0.0f, 1.0f, 0.0f);

	if (mode == CGV_VISUALIZE && !showBoundingBox)
		gluCylinder(gluNewQuadric(), 0.4f, 0.4f, 3.0f, 32, 32);
	else {
		glScaled(1, 1, 3);
		glTranslated(0, 0, 0.5);
		glutSolidCube(1);
	}
	
}

void cgvScene3D::arm1(renderMode mode) {
	int objectNumber = 2;
	if (selectedObject == objectNumber)
		glColor3f(255.0f, 255.0f, 0.0f);
	else
		glColor3f(objectColor[objectNumber][0], objectColor[objectNumber][1], objectColor[objectNumber][2]);

	//Giro de brazos (inicializamos con q y rotamos con w)
	glRotatef(rotatingArms, 1.0f, 0.0f, 0.0f);
	if (armsRotated)
		glRotatef(45.0f, 0.0f, 0.0f, 1.0f);
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);

	if (mode == CGV_VISUALIZE && !showBoundingBox)
		gluCylinder(gluNewQuadric(), 0.4f, 0.4f, 3.0f, 32, 32);
	else {
		glScaled(1, 1, 3);
		glTranslated(0, 0, 0.5);
		glutSolidCube(1);
	}

}

void cgvScene3D::arm2(renderMode mode) {
	int objectNumber = 3;
	if (selectedObject == objectNumber)
		glColor3f(255.0f, 255.0f, 0.0f);
	else
		glColor3f(objectColor[objectNumber][0], objectColor[objectNumber][1], objectColor[objectNumber][2]);

	glRotatef(90.0f, 0.0f, -1.0f, 0.0f);
	
	if (mode == CGV_VISUALIZE && !showBoundingBox)
		gluCylinder(gluNewQuadric(), 0.4f, 0.4f, 3.0f, 32, 32);
	else {
		glScaled(1, 1, 3);
		glTranslated(0, 0, 0.5);
		glutSolidCube(1);
	}
}


void cgvScene3D::body(renderMode mode) {
	int objectNumber = 4;
	if (selectedObject == objectNumber)
		glColor3f(255.0f, 255.0f, 0.0f);
	else
		glColor3f(objectColor[objectNumber][0], objectColor[objectNumber][1], objectColor[objectNumber][2]);

	if (mode == CGV_VISUALIZE && !showBoundingBox)
		gluCylinder(gluNewQuadric(), 0.4f, 0.4f, 5.0f, 32, 32);
	else {
		glScaled(1, 1, 5);
		glTranslated(0, 0, 0.5);
		glutSolidCube(1);
	}
}

void cgvScene3D::head(renderMode mode) {
	int objectNumber = 5;
	if (selectedObject == objectNumber)
		glColor3f(255.0f, 255.0f, 0.0f);
	else
		glColor3f(objectColor[objectNumber][0], objectColor[objectNumber][1], objectColor[objectNumber][2]);

	if (mode == CGV_VISUALIZE && !showBoundingBox)
		gluSphere(gluNewQuadric(), 1, 100000, 100000);
	else {
		glutSolidCube(2);
	}
	
}

void cgvScene3D::nose(renderMode mode) {
	int objectNumber = 6;
	if (selectedObject == objectNumber)
		glColor3f(255.0f, 255.0f, 0.0f);
	else
		glColor3f(objectColor[objectNumber][0], objectColor[objectNumber][1], objectColor[objectNumber][2]);

	glTranslated(0, 0, 1);

	if (mode == CGV_VISUALIZE && !showBoundingBox)
		gluCylinder(gluNewQuadric(), 0.2, 0.0001f, 3.0*noseFactor, 32, 32);
	else {
		glScaled(1,1, 7 * noseFactor);
		glTranslated(0, 0, 0.2);
		glutSolidCube(0.4);
	}
	
}

void cgvScene3D::hat(renderMode mode) {
	int objectNumber = 7;
	if (selectedObject == objectNumber)
		glColor3f(255.0f, 255.0f, 0.0f);
	else
		glColor3f(objectColor[objectNumber][0], objectColor[objectNumber][1], objectColor[objectNumber][2]);

	glScalef(2, 2, 2);
	glRotatef(-15.0f, 1.0f, 0.0f, 0.0f);
	

	if (mode == CGV_VISUALIZE && !showBoundingBox) {
		glRotatef(45.0f, 0.0f, 1.0f, 0.0f);
		float vertex[18] = { 1,0,0,   -1,0,0,   0,0,1,   0,0,-1,   0,1,0,   0,-1,0 };
		unsigned int index[18] = { 0,4,2,      0,3,4,      1,2,4,      3,1,4,    3,0,2,  3,2,1 };

		glEnableClientState(GL_VERTEX_ARRAY);
		glVertexPointer(3, GL_FLOAT, 0, vertex);

		glDrawElements(GL_TRIANGLES, 18, GL_UNSIGNED_INT, index);
		glDisableClientState(GL_VERTEX_ARRAY);
	}
	else {
		glScaled(1.5, 1, 1);
		glTranslated(0, 0.4, 0);
		glutSolidCube(1);
	}
}


