#include <cstdlib>
#include <stdio.h>

#include "cgvInterface.h"


extern cgvInterface interface; // the callbacks must be static and this object is required to access to the variables of the class                  

// Constructor and destructor methods -----------------------------------

cgvInterface::cgvInterface():camType(CGV_PARALLEL)  {
//// TODO: Section D: initialize the attributes to select objects by list of impacts


	mode = CGV_VISUALIZE;
	pressed_button = false;
}

cgvInterface::~cgvInterface () {}


// Public methods ----------------------------------------
void cgvInterface::create_world(void) {
	currentCam = 0 ;
	camera[0] = cgvCamera(cgvPoint3D(3.0, 2.0, 4), cgvPoint3D(0, 0, 0), cgvPoint3D(0, 1.0, 0));
	camera[0].setParallelParameters(1 * 5, 1 * 5, 0.1, 200);

	// TODO: Practice 2B.b: Define top, lateral and front cameras, respectively
	camera[1] = cgvCamera(cgvPoint3D(3.0, 0, 0), cgvPoint3D(0, 0, 0), cgvPoint3D(0, 1.0, 0));
	camera[1].setParallelParameters(1 * 5, 1 * 5, 0.1, 200);
	camera[2] = cgvCamera(cgvPoint3D(0, 4, 0), cgvPoint3D(0, 0, 0), cgvPoint3D(1, 0, 0));
	camera[2].setParallelParameters(1 * 5, 1 * 5, 0.1, 200);
	camera[3] = cgvCamera(cgvPoint3D(0, 0, 4), cgvPoint3D(0, 0, 0), cgvPoint3D(0, 1.0, 0));
	camera[3].setParallelParameters(1 * 5, 1 * 5, 0.1, 200);

}

void cgvInterface::configure_environment(int argc, char** argv, 
			                       int _width_window, int _height_window, 
			                       int _pos_X, int _pos_Y, 
													 string _title)
													 {
	// initialization of the interface variables																	
	width_window = _width_window;
	height_window = _height_window;

	// initialization of the display window
	glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
  glutInitWindowSize(_width_window,_height_window);
  glutInitWindowPosition(_pos_X,_pos_Y);
	glutCreateWindow(_title.c_str());

	glEnable(GL_DEPTH_TEST); // enable the removal of hidden surfaces by using the z-buffer
  glClearColor(1.0,1.0,1.0,0.0); // define the background color of the window

	glEnable(GL_LIGHTING); // enable the lighting of the scene
  glEnable(GL_NORMALIZE); // normalize the normal vectors required by the lighting computation. 

  create_world(); // create the world (scene) to be rendered in the window
}

void cgvInterface::init_rendering_loop() {
	glutMainLoop(); // initialize the visualization loop of OpenGL
}

void cgvInterface::set_glutKeyboardFunc(unsigned char key, int x, int y) {
  switch (key) {
 ////// Section B: include here the change of the camera to show the top, lateral, side and perspective views
  case 'v': // change the current camera to show these views: panoramic, top, front and lateral view
	  interface.currentCam = (interface.currentCam + 1) % 4;
	  break;

////// TODO: Section C: include here the modification of the degree of freedom by clicking the corresponding keys

  case 'x': // TODO: Section A: rotate positive X 
	  interface.scene.incr_rotX(10);
	  break;
  case 'y': // TODO: Section A: rotate positive Y 
	  interface.scene.incr_rotY(10);
	  break;
  case 'z': // TODO: Section A: rotate positive Z 
	  interface.scene.incr_rotZ(10);
	  break;
    case 'a': // enable/disable the visualization of the axes
			interface.scene.set_axes(interface.scene.get_axes()?false:true);
			break;


	case 'q': //Make the right arm move in order to be able to rotate
		interface.scene.set_armsRotated(!interface.scene.get_armsRotated());
		break;
	case 'w': // Rotate the right Arm if armsRotated==true
		interface.scene.incr_rotatingArms(10);
		break;

	case 'l': // Move the legs like dancing
		if (interface.scene.get_legDir()) {
			interface.scene.incr_rotatingLegs(10);
			if (interface.scene.get_rotatingLegs() > 30)
				interface.scene.set_legDir(false);
		}
		else {
			interface.scene.incr_rotatingLegs(-10);
			if (interface.scene.get_rotatingLegs() < -30)
				interface.scene.set_legDir(true);
		}
		break;

	case 'b': 
		interface.scene.set_showBoundingBox(!interface.scene.get_showBoundingBox());
		break;

	case 'n':
		interface.scene.change_noseFactor();
		break;

	  break;
    case 27: // Escape key to exit
      exit(1);
    break;
  }
	glutPostRedisplay(); // renew the content of the window
}

void cgvInterface::set_glutReshapeFunc(int w, int h) {
  // dimension of the viewport with a new width and a new height of the display window 


  // store the new values of the viewport and the display window. 
  interface.set_width_window(w);
  interface.set_height_window(h);

  // Set up the kind of projection to be used
  interface.camera[interface.currentCam].apply();

}

void cgvInterface::set_glutDisplayFunc() {

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the window and the z-buffer

	// set up the viewport
	glViewport(0, 0, interface.get_width_window(), interface.get_height_window());

	// Section D: check the mode before applying the camera and projection transformations,
	if (interface.mode == CGV_SELECT) {
	  // Section D: if it is in the OpenGL selection mode disable the lighting to use the color buffer technique
            interface.init_selection();
	}
	// Apply the camera and projection transformations 
	interface.camera[interface.currentCam].apply();

	// Render the scene
	interface.scene.render(interface.mode);

	if (interface.mode == CGV_SELECT) {
		// Section D: exit the selection mode and process the information in the color buffer
		interface.finish_selection(); 
		glutPostRedisplay(); 
	}
        else {
		// refresh the window
		glutSwapBuffers(); // it is used instead of glFlush(), to avoid flickering
	}	

}


void cgvInterface::set_glutMouseFunc(GLint button,GLint state,GLint x,GLint y) {
// Section D: check if the left button of the mouse has been clicked 
	if (button == GLUT_LEFT_BUTTON) {
		// Section D: Store the button that has been pressed or released. If it has been clicked, then change to selection mode (CGV_SELECT)
		interface.pressed_button = state == GLUT_DOWN;
		if(interface.pressed_button)
			interface.mode = CGV_SELECT;
		else
			interface.scene.set_selectedObject(-1);

		// Section D: Save the position of the pixel when the mouse was clicked
		interface.cursorX = x;
		interface.cursorY = y;

	}

// Section D: Redraw the content of the display window 
	glutPostRedisplay();

}


void cgvInterface::set_glutMotionFunc(GLint x,GLint y) {
// TODO: Section F: if the mouse button is pressed and there is a selected object, then check the selected object 
// and the position of the mouse to update the corresponding degree of freedom of the model accordingly 
	if (interface.pressed_button && interface.scene.get_selectedObject()>-1) {
		int difX = interface.cursorX - x;
		int difY = interface.cursorY - y;

		// 0 - LEG1 | 1 - LEG2 | 2 - ARM1 | 3 - ARM2 | 4 - BODY | 5 - HEAD | 6 - NOSE | 7 - HAT
		switch (interface.scene.get_selectedObject())
		{
		case 0:

		case 1:
			if (interface.scene.get_rotatingLegs() < 40 && difX > 0)
				interface.scene.incr_rotatingLegs(difX);
			
			if (interface.scene.get_rotatingLegs() > -40 && difX < 0)
				interface.scene.incr_rotatingLegs(difX);
			
			break;
		case 2:
			if (difX != 0 && difY != 0) {
				if (!interface.scene.get_armsRotated()) {
					interface.scene.set_armsRotated(true);
				}
				interface.scene.incr_rotatingArms(abs(difX) + abs(difY));
			}
			break;
		case 5:
			interface.scene.incr_rotX2(-difY);
			interface.scene.incr_rotY2(-difX);
			break;
		case 6:
			interface.scene.incr_noseFactor((float)difX);
		default:
			break;
		}
	}
// Section F: Save the new position of the mouse
	interface.cursorX = x;
	interface.cursorY = y;

// Section F: Redraw the content of the display window 
	glutPostRedisplay();
}

void cgvInterface::init_callbacks() {
	glutKeyboardFunc(set_glutKeyboardFunc);
	glutReshapeFunc(set_glutReshapeFunc);
	glutDisplayFunc(set_glutDisplayFunc);

	glutMouseFunc(set_glutMouseFunc);
	glutMotionFunc(set_glutMotionFunc); 	
}


void cgvInterface::init_selection() {
	// Section D: Disable lighting. 
     glDisable(GL_LIGHTING);

}

void cgvInterface::finish_selection() {

// Section D: Prepare for reading the color buffer
	glReadBuffer(GL_BACK);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glPixelStorei(GL_PACK_ALIGNMENT, 1);

	GLfloat pixels[3] = { 0, 0, 0 };

	// TODO: Section D: Use the function glReadPixels to get the color at a given position of the window. 
	glReadPixels(interface.cursorX, interface.height_window - interface.cursorY, 1, 1, GL_RGB, GL_FLOAT,pixels);
	
	// Section D: Call the assignSelection method of the scene class to compute the selected part of the model, if any. 
	interface.scene.assignSelection(pixels);  
	interface.mode = CGV_VISUALIZE;

    glEnable(GL_LIGHTING);
}








